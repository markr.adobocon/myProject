DROP DATABASE adobo;

CREATE DATABASE adobo;

USE adobo;

/*Table structure for table `product`*/

DROP TABLE IF EXISTS product;

CREATE TABLE product (

	id serial NOT NULL,
	name varchar(20) NOT NULL,
	price decimal(6,2) NOT NULL,
	uom varchar(15) DEFAULT NULL,
	base int(10) DEFAULT NULL,
	code varchar(10) DEFAULT NULL,
	PRIMARY KEY (id)

);

INSERT INTO product (name, price, uom, base, code) values

('Chicken Adobo Meat', 313.58, '1050g/pack', 1050, 'FG-M037'),
('Pork Adobo Meat', 419.32, '1050g/pack', 1050, 'FG-M041'),
('Adobo Flakes', 309.86, '615g/pack', 615, 'FG-M001'),
('Adobo Pao', 80.58, '10pcs/pack', 10, 'FG-M003'),
('Boiled Beef', 465.79, '750g/pack', 750, 'FG-M007');